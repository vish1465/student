
const constants = {
    studentdata: [
        {
            "firstname": "Vishal",
            "middlename": null,
            "lastname": "Patil",
            "dob": "1990-07-23T18:30:00.000Z",
            "gender": "Male",
            "emailaddress": "test@yopmail.com",
            "contactnumber": "354544554",
            "city": "Pune",
            "state": "Maharastra",
            "postalcode": "41214"
        },
        {
            "firstname": "Rohit",
            "middlename": null,
            "lastname": "Patil",
            "dob": "1990-02-05T18:30:00.000Z",
            "gender": "Male",
            "emailaddress": "testsdf@yopmail.com",
            "contactnumber": "4353434",
            "city": "Pune",
            "state": "Maharastra",
            "postalcode": "41214"
        },
        {
            "firstname": "Neha",
            "middlename": null,
            "lastname": "Patil",
            "dob": "1992-01-02T18:30:00.000Z",
            "gender": "Female",
            "emailaddress": "testsdf@yopmail.com",
            "contactnumber": "5446878",
            "city": "Pune",
            "state": "Maharastra",
            "postalcode": "41214"
        },
        {
            "firstname": "Samiksha",
            "middlename": null,
            "lastname": "Patil",
            "dob": "1995-01-02T18:30:00.000Z",
            "gender": "Female",
            "emailaddress": "testsdf@yopmail.com",
            "contactnumber": "5446878",
            "city": "Pune",
            "state": "Maharastra",
            "postalcode": "41214"
        },
        {
            "firstname": "Pranit",
            "middlename": null,
            "lastname": "Patil",
            "dob": "1996-01-02T18:30:00.000Z",
            "gender": "Male",
            "emailaddress": "pranit@yopmail.com",
            "contactnumber": "9876564",
            "city": "Pune",
            "state": "Maharastra",
            "postalcode": "41214"
        }
    ]
}


module.exports = constants;