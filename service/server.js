require('dotenv').config();
const express = require('express');
const app = express();
const http = require('http');
const errorHandler = require(process.cwd() + '/utility/errorhandler');
const bodyParser = require('body-parser');
const studentLogger = require(process.cwd() + '/utility/logger').logger;

app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
//expose services
require('./student/studentservice')(app);

app.use(errorHandler());

const server = http.createServer(app);
server.listen(process.env.PORT);
studentLogger.info(`Service is running on PORT ${process.env.PORT}`)
console.log(`listening on port ${process.env.PORT}`);

module.exports = server;