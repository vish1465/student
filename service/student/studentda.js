const dbhelper = require('../dataacess/dbhelper');

let studentDa = () => { }

studentDa.getStudentDetails = async (req, res, next) => {
    let text = `select firstname, middlename, lastname, dob, gender, emailaddress, contactnumber, city, state, postalcode from studentdetails where isactive=true`;
    let values = [];
    let result = await dbhelper.executeNonQuery(text, values, next);
    return result;
}

studentDa.insertStudentDetails = async(req, res, next) => {
    const text = 'INSERT INTO public.studentdetails(firstname, middlename, lastname, dob, gender, emailaddress, contactnumber, city, state, postalcode, isactive) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9,$10, $11) RETURNING Id;'
    const values = [req.body.firstname, req.body.middlename, req.body.lastname, req.body.dob, req.body.gender, req.body.emailaddress, req.body.contactnumber, req.body.city, req.body.state, req.body.postalcode, true]
    let result = await dbhelper.executeNonQuery(text, values, next);
    return result;

}

module.exports.studentDa = studentDa;