
const { studentBusiness } = require(process.cwd() + '/student/studentbusiness')

module.exports = app => {

    app.get('/', (req, res) => {
        res.status(200).send("Hello world");
    })

    app.get('/getstudentdetails', async (req, res, next) => {
        let response = await studentBusiness.getStudentDetails(req, res, next);
        res.status(200).send(response);
    })

    app.post('/insertstudentdetails', async(req, res, next) => {
        let response = await studentBusiness.insertStudentDetails(req, res, next);
        res.status(200).send(response);
    })
}