const { commonFunctions } = require(process.cwd() + '/utility/commonfunctions');
const { studentDa } = require(process.cwd() + '/student/studentda');
const constants = require('../configuration/constants');

let studentBusiness = () => { }

studentBusiness.getStudentDetails = async (req, res, next) => {
    if(process.env.ISFLAT_DATA){
        return commonFunctions.createResponse({ studentdetails: constants.studentdata }, commonFunctions.resultStatus.SUCCESS, "success");
    }
    let result = await studentDa.getStudentDetails(req, res, next);
    if (result && result.rowCount > 0) {
        return commonFunctions.createResponse({ studentdetails: result.rows }, commonFunctions.resultStatus.SUCCESS, "success");
    }
    else {
        return commonFunctions.createResponse({}, commonFunctions.resultStatus.NORECORDS, "No records found");
    }
}

studentBusiness.insertStudentDetails = async (req, res, next) => {
    let result = await studentDa.insertStudentDetails(req, res, next);
    if (result && result.rowCount > 0) {
        return commonFunctions.createResponse({}, commonFunctions.resultStatus.SUCCESS, "success");
    }
    else {
        return commonFunctions.createResponse({}, commonFunctions.resultStatus.FAIL, "error");
    }
}

module.exports.studentBusiness = studentBusiness;