const {Pool} = require('pg');
const config = require('../configuration/config.js');

const db = () => {}

db.executeNonQuery = async(queryText, values, next)=>{
    let pool = new Pool(config.postgresDbDetails);
    let response = await pool.query(queryText, values);
    pool.end();
    return response;
}

module.exports = db;