const commonFunctions = () => { }

commonFunctions.resultStatus = {
    FAIL: 0,
    SUCCESS: 1,
    NORECORDS: 2
}

commonFunctions.createResponse = (data, status, description) => {
    let result = {}
    result.data = data;
    result.status = status;
    result.description = description;
    return result;

}

module.exports.commonFunctions = commonFunctions;
