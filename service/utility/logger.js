const { createLogger, format, transports, transport } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(info => {
    return `${new Date(info.timestamp)} [${info.label}] ${info.level}: ${info.message}`;
})

const configValues = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    verbose: 4,
    debug: 5,
    silly: 6
}

const logger = createLogger({
    levels: configValues,
    format: combine(
        label({label: 'Student'}),
        timestamp(),
        myFormat
    ),
    transports: [
        new transports.Console(),
        new transports.File({
            filename: 'student.log',
            maxsize: '10mb'
        })   
    ]
})

module.exports.logger = logger;