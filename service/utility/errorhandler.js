
const StudentLogger = require(process.cwd() + '/utility/logger').logger;

module.exports = () => {
    return (err, req, res, next) => {
        StudentLogger.error("error message " + err.message);
        StudentLogger.error("stackTrace " + err.stack);
        let resultStatus = "Something went wrong";
        res.status(err.status || 500).send(resultStatus);
    }
}
