import React from "react";
import { shallow } from "enzyme";
import BasicModal from "./BasicModal";

describe("BasicModal", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<BasicModal />);
    expect(wrapper).toMatchSnapshot();
  });
});
