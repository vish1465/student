import './App.css';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import React from 'react';


// Style
const paperStyle = {
  textAlign: 'center',
  margin: '10px',
  padding: '10px'
}

//Static data
const rows = [
  {
    "firstname": "Vishal",
    "middlename": null,
    "lastname": "Patil",
    "dob": "1990-07-23T18:30:00.000Z",
    "gender": "Male",
    "emailaddress": "test@yopmail.com",
    "contactnumber": "354544554",
    "city": "Pune",
    "state": "Maharastra",
    "postalcode": "41214"
  },
  {
    "firstname": "Rohit",
    "middlename": null,
    "lastname": "Patil",
    "dob": "1990-02-05T18:30:00.000Z",
    "gender": "Male",
    "emailaddress": "testsdf@yopmail.com",
    "contactnumber": "4353434",
    "city": "Pune",
    "state": "Maharastra",
    "postalcode": "41214"
  },
  {
    "firstname": "Neha",
    "middlename": null,
    "lastname": "Patil",
    "dob": "1992-01-02T18:30:00.000Z",
    "gender": "Female",
    "emailaddress": "testsdf@yopmail.com",
    "contactnumber": "5446878",
    "city": "Pune",
    "state": "Maharastra",
    "postalcode": "41214"
  },
  {
    "firstname": "Samiksha",
    "middlename": null,
    "lastname": "Patil",
    "dob": "1995-01-02T18:30:00.000Z",
    "gender": "Female",
    "emailaddress": "testsdf@yopmail.com",
    "contactnumber": "5446878",
    "city": "Pune",
    "state": "Maharastra",
    "postalcode": "41214"
  },
  {
    "firstname": "Pranit",
    "middlename": null,
    "lastname": "Patil",
    "dob": "1996-01-02T18:30:00.000Z",
    "gender": "Female",
    "emailaddress": "pranit@yopmail.com",
    "contactnumber": "9876564",
    "city": "Pune",
    "state": "Maharastra",
    "postalcode": "41214"
  }
];

// SERVICE URL
const API_URL = 'http://localhost:6065/getstudentdetails';

//Student column
const studentColumnDef = {
  studentGrid: {
    columns: ['Name', 'Date of Birth', 'Gender', 'City', 'State', 'PostalCode']
  }
}

//APP
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      headertitle: 'Student Details',
      studentdetails: {
        data: [],
        columndef: studentColumnDef.studentGrid.columns
      },
    }
  }

  componentDidMount() {
    this.getStudentdetails();
  }

  //getStudentDetails
  getStudentdetails = async () => {
    try {
      let studentData = [];
      const request = await fetch(API_URL, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const res = await request.json();
      if (res && res.status === 1 && res.data && res.data.studentdetails && res.data.studentdetails.length > 0) {
        studentData = res.data.studentdetails;

        this.setState({
          studentdetails: {
            data: studentData,
            columndef: studentColumnDef.studentGrid.columns
          }
        })
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div style={{ flexGrow: 1 }}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper style={paperStyle} elevation={3}><h2>{this.state.headertitle}</h2></Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper style={paperStyle} elevation={3}>
              <Button variant="contained" color="primary" style={{ float: 'right', margin: '5px' }}>
                ADD
              </Button>
              {this.state.studentdetails && this.state.studentdetails.data && this.state.studentdetails.data.length > 0 ?
                <StudentTable data={this.state.studentdetails.data}
                  columndef={this.state.studentdetails.columndef}
                />
                : 'No data available'}
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

// Table component
const StudentTable = (props) => {
  const data = props;
  debugger
  const moment = require('moment');
  debugger;
  return (
    <TableContainer component={Paper}>
      <Table style={{ minWidth: 600 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {props.columndef.map((row) => (<TableCell>{row}</TableCell>))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.data.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.firstname} {row.lastname}
              </TableCell>
              <TableCell>{row.dob ? moment(row.dob).format('YYYY/MM/DD') : ''}</TableCell>
              <TableCell>{row.gender}</TableCell>
              <TableCell>{row.city}</TableCell>
              <TableCell>{row.state}</TableCell>
              <TableCell>{row.postalcode}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>)
}

export default App;
